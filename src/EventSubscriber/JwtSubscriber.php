<?php

namespace Drupal\page_preview\EventSubscriber;

use Drupal\Core\Session\AccountInterface;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class JwtSubscriber.
 */
class JwtSubscriber implements EventSubscriberInterface
{

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The current user.
   */
  public function __construct(AccountInterface $user)
  {
    $this->currentUser = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
    $events[JwtAuthEvents::GENERATE][] = ['setPathAuthClaims'];
    return $events;
  }

  /**
   * Add claims for JwtPathAuth that allow authenticating
   * using the "jwt" query param. Used by iFrames as they can't
   * include an Authorization header.
   *
   * @param \Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent $event
   *   The event.
   */
  public function setPathAuthClaims(JwtAuthGenerateEvent $event)
  {
    $event->addClaim(['drupal', 'path_auth', 'uid'], $this->currentUser->id());
    $event->addClaim(['drupal', 'path_auth', 'path'], '/frontend-editing/');
  }
}
