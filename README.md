# Frontend editing module for a headless nextjs frontend

## Requirements for the frontend
- [npm module](https://www.npmjs.com/package/@burst/frontend-editing)

## Requirements for the backend
- [paragraphs module](https://www.drupal.org/project/paragraphs)
- [allow site iframing module](https://www.drupal.org/project/allow_iframed_site)
- [jwt authentication module](https://www.drupal.org/project/jwt_auth)

## Configuration
add these values to the .env file in the root of the project.

```dotenv
NEXTJS_DOMAIN=http://localhost:3000
NEXTJS_REVALIDATE_SECRET=FAKESECRET

PREVIEW_URL=http://localhost:3000

JWT_TOKEN=FAKETOKEN
```
```if a jwt already exists, it is posstible that your jwt_token has a different way of configuration, for example in another file.```

### steps

1. If a Page Preview module already exists, make sure to disable this. 
2. Run  ```composer require 'drupal/fe_editing_nextjs:^1.0@beta'```
3. Make sure this module, the JWT Path Authentication module and the allow site iframing module are enabled. 
4. go to admin/settings/system/JWT_authentication and click the tab JWT Path Authentication, add the following path: ```/frontend-editing/```
5. go to admin/config/system/allowed_iframed_sites and add the url from the frontend. (or localhost if local)
6. go to admin/config/system/keys and add a new key with the following settings:
  - ```key type = JWT HMAC Key```
  - ```JWT Algorithm = HMAC using SHA-256 (HS256)```
  - ```Key Provider Environment```
  - ```Evironment Variable = JWT_TOKEN```
7. check the patches folder for patches applied to the /includes/html.theme file, if the following code exists, in there.. delete it (or disable the patch).
```php
$route_name = \Drupal::routeMatch()->getRouteName();
   if ($route_name === 'entity.node.canonical') {
     $url = Url::fromRoute('<current>')->toString();
     $url = Url::fromUserInput("/api/preview?path=$url")->setAbsolute()->toString();
     $fe_url = Settings::get('burst_theme_frontend_url')($url);

     $override = \Drupal::state()->get('fe_url_override');
     if ($override) {
       $path = Url::fromRoute('<current>')->toString();
       $fe_url = $override . "/api/preview?path=$path";
     }

     if (function_exists('page_preview_token_generate')) {
       $fe_url = $fe_url . '&token=' . page_preview_token_generate();
     }

     if ($url !== $fe_url) {
       $variables['page']['content']['burst_theme_content'] = [
         '#type' => 'inline_template',
         '#template' => '
                 <div id="frontend-iframe-container">
                   <a href="' . $fe_url . '" class="button button--primary" target="_blank">' . t('Preview page') . '</a>
                   <iframe src="{{ url }}" id="frontend-iframe"></iframe>
                 </div>
               ',
         '#context' => [
           'url' => $fe_url,
         ],
         '#attached' => [
           'library' => ['burst_theme/frontend-iframe']
         ]
       ];
     }
```