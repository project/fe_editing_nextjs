<?php

namespace Drupal\frontend_editing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines EntityEditFormController controller.
 */
class EntityEditFormController extends ControllerBase
{

  /**
   * Render the entity edit form.
   *
   * @return array
   *   Return markup array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function renderForm(EntityInterface $entity, string $langcode)
  {
    /** @var \Drupal\Core\Entity\EntityFormBuilder $form_builder */
    $form_builder = \Drupal::service('entity.form_builder');

    $form = $form_builder->getForm($entity, 'edit', ['langcode' => $langcode]);

    unset($form['content_translation']);
    unset($form['actions']['delete_translation']);

    $form['#attached']['library'][] = 'frontend_editing/frontend_editing.form';

    return $form;
  }
}
